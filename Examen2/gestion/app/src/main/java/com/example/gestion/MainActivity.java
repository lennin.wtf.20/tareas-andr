package com.example.gestion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gestion.R;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    EditText nom;
    RadioGroup rdgroup;
    RadioButton radioButton,rbPrim,rbSeg,rbTercer,rbCuarto,rbQuinto,rbSexto;
    CheckBox chkGestion,chkDesarrollo,chkAnalisis,chkLenguaje,chkDiseño,chkBase;
    Button btnRegistrar;
    TextView tvResultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nom = (EditText) findViewById(R.id.edtNombre);
        chkBase = (CheckBox) findViewById(R.id.chkBase);
        chkDiseño = (CheckBox) findViewById(R.id.chkDiseño);
        chkLenguaje = (CheckBox) findViewById(R.id.chkLenguaje);
        chkAnalisis = (CheckBox) findViewById(R.id.chkAnalisis);
        chkDesarrollo = (CheckBox) findViewById(R.id.chkDesarrollo);
        chkGestion = (CheckBox) findViewById(R.id.chkGestion);

        rbPrim = (RadioButton) findViewById(R.id.rbPrim);
        rbSeg = (RadioButton) findViewById(R.id.rbSeg);
        rbTercer = (RadioButton) findViewById(R.id.rbTercer);
        rbCuarto = (RadioButton) findViewById(R.id.rbCuarto);
        rbQuinto = (RadioButton) findViewById(R.id.rbQuinto);
        rbSexto = (RadioButton) findViewById(R.id.rbSexto);
        rdgroup = (RadioGroup) findViewById(R.id.rdgroup);


        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i= new Intent(MainActivity.this, resultado.class);
                i.putExtra("nombre", nom.getText().toString());



                startActivity(i);

                int radioId= rdgroup.getCheckedRadioButtonId();
                radioButton = findViewById(radioId);


                if (v.getId() ==R.id.btnRegistrar){

                    checkButton(v);



                }



            }
        });
    }


    public void checkButton(View v){



        String cad="Seleccionado \n";


        if (rbPrim.isChecked()==true){
            cad += rbPrim.getText().toString()+",";

        }if (rbSeg.isChecked()==true){
            cad +=rbSeg.getText().toString()+",";

        }if (rbTercer.isChecked()==true){
            cad +=rbTercer.getText().toString()+",";

        }if (rbCuarto.isChecked()==true){
            cad +=rbCuarto.getText().toString()+",";

        }if (rbQuinto.isChecked()==true){
            cad += rbQuinto.getText().toString()+",";

        }if (rbSexto.isChecked()==true){
            cad += rbSexto.getText().toString()+",";

        }

        String cod=" \n";
        if ( chkBase.isChecked()==true){
            cod += chkBase.getText().toString()+",";



        }if (chkDiseño.isChecked()==true){
            cod += chkDiseño.getText().toString()+",";

        }if (chkLenguaje.isChecked()==true){
            cod += chkLenguaje.getText().toString()+",";

        }if (chkAnalisis.isChecked()==true){
            cod += chkAnalisis.getText().toString()+",";

        }if (chkDesarrollo.isChecked()==true){
            cod += chkDesarrollo.getText().toString()+",";

        }if (chkGestion.isChecked()==true){
            cod += chkGestion.getText().toString()+",";

        }


        Intent intent = new Intent(this, resultado.class);
        String message = nom.getText().toString();
        intent.putExtra("nombre", message);
        intent.putExtra("semestre",cad);
        intent.putExtra("cursos",cod);

      startActivity(intent);

    }



}

