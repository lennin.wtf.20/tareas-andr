package com.example.gestion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class resultado extends AppCompatActivity {

    private TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        Intent intent = getIntent();
        String message = intent.getStringExtra("nombre");
        String semestre= intent.getStringExtra("semestre");
        String cursos= intent.getStringExtra("cursos");

        TextView nombre = findViewById(R.id.nombre);
        TextView semestres = findViewById(R.id.semestre);
        TextView curso = findViewById(R.id.cursos);

        nombre.setText( message);
        semestres.setText( semestre);
        curso.setText(cursos);
    }
}




