package utils;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Utils {
    public static final String TAG = Utils.class.getName();
    public static final String CADENA_VACIA = "";

    //Metodos genéricos
    public static boolean isTextoValido(View view){
        TextInputEditText dato = (TextInputEditText) view;
        String texto = dato.getText().toString();
        Log.d(TAG,">>>Validar texto: "+texto);
        return (Utils.CADENA_VACIA.equals(texto)?false:true);
    }

    public static void mostrarMensaje(Context context, String mensaje){
        Toast.makeText(context, mensaje, Toast.LENGTH_SHORT).show();
    }
}
