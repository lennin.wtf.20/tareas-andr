package com.example.avengers.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.avengers.R;

public class RetadoresActivity extends AppCompatActivity {

    ImageButton iroman,cap,hulk,dr,dead;
    Button btnButton;
    CheckBox thor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retadores);

         iroman = findViewById(R.id.iroman);
         cap = findViewById(R.id.cap);
         thor = findViewById(R.id.thor);
         hulk = findViewById(R.id.hulk);
         dr = findViewById(R.id.dr);
         dead = findViewById(R.id.dead);
         btnButton = findViewById(R.id.btnButton);




        Intent intent = getIntent();
        String message = intent.getStringExtra("nombre");
        String messa= intent.getStringExtra("alias");
        String genero= intent.getStringExtra("genero");

        TextView nombre = findViewById(R.id.tvnombre);

        nombre.setText( "Hola Sr"+ genero+" "+message+ ","+ messa+"\n Bienvenido a la batalla final \n elige su personaje");




    }
    public void batalla(View v){
        String rad="";
        if (thor.isChecked()==true){
            rad += thor.getText().toString();
        }
        Intent intent = new Intent(this, LuchaActivity.class);
        intent.putExtra("heroe",rad);
        startActivity(intent);
    }
}
