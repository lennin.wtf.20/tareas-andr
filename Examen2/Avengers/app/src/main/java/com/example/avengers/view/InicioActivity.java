package com.example.avengers.view;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.avengers.R;

import utils.Utils;

public class InicioActivity extends AppCompatActivity {
    public static final String TAG = InicioActivity.class.getName();
    TextInputEditText edtNombres;
    TextInputEditText edtAlias;
    Button btnRetadores;
    RadioButton rbtM;
    RadioButton rbtF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,">>>Ingrese al metodo - onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);



        edtNombres = findViewById(R.id.edtNombres);
        edtAlias = findViewById(R.id.edtAlias);
        btnRetadores = findViewById(R.id.btnRetadores);
        rbtM = findViewById(R.id.rbtM);
        rbtF = findViewById(R.id.rbtF);

        this.obtenerControler(null);

    }

    public void checkButton (View v){

    }
    @Override
    protected void onStart() {
        Log.d(TAG,">>>Ingrese al metodo - onStart()");
        super.onStart();
    }
    @Override
    protected void onResume() {
        Log.d(TAG,">>>Ingrese al metodo - onResume()");
        super.onResume();
    }
    @Override
    protected void onPause() {
        Log.d(TAG,">>>Ingrese al metodo - onPause()");
        super.onPause();
    }
    @Override
    protected void onStop() {
        Log.d(TAG,">>>Ingrese al metodo - onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG,">>>Ingrese al metodo - onDestroy()");
        super.onDestroy();
    }


    public void elegirRetadores(View v) {
        Log.d(TAG, ">>>inicio de metodo elegirRetadores()");
        String mensaje = Utils.CADENA_VACIA;
        boolean flag = false;
        String rad="";
        if (rbtM.isChecked()==true){
            rad = " ";
        } else if (rbtF.isChecked()==true){
            rad = "a";
        }
        Intent intent = new Intent(this, RetadoresActivity.class);
        String message = edtNombres.getText().toString();
        String messa = edtAlias.getText().toString();
        intent.putExtra("nombre", message);
        intent.putExtra("alias", messa);
        intent.putExtra("genero",rad);
        startActivity(intent);



    }

    public void obtenerControler(View v){
        edtNombres = findViewById(R.id.edtNombres);
        edtAlias = findViewById(R.id.edtAlias);
        btnRetadores = findViewById(R.id.btnRetadores);
        rbtM = findViewById(R.id.rbtM);
        rbtF = findViewById(R.id.rbtF);
    }


}
