package com.example.avengers.view;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.avengers.R;

public class GrindViewHeroes extends BaseAdapter {

     private Context mContext;
        String[]nombre;
        String[] vida;
        String [] ataque;
        String [] defensa;

        private ArrayList<Integer>listPhoto = new ArrayList<Integer>();
        public GrindViewHeroes(Context c , String[] nombre,String[] vida,String[] ataque,String[] defensa){
            mContext = c;
            listPhoto.add(R.drawable.iroman);
            listPhoto.add(R.drawable.capitan);
            listPhoto.add(R.drawable.thor);
            listPhoto.add(R.drawable.hulk);
            listPhoto.add(R.drawable.dr);
            listPhoto.add(R.drawable.dead);

        }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource( listPhoto.get(position) );
        imageView.setScaleType( ImageView.ScaleType.CENTER_CROP );
        imageView.setLayoutParams( new GridView.LayoutParams(100, 120) );
        return imageView;
    }
}
