package com.castillo.restaurante;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.castillo.restaurante.GridViewBebidas;
import com.castillo.restaurante.GridViewPlatos;

import org.w3c.dom.Text;

class MainActivity extends AppCompatActivity implements View.OnClickListener{
    GridViewPlatos adapter;
    GridViewBebidas adapterb;
    TextView edtTitulo;
    TextView edtTitulob;
    TextView edtPrecio;
    TextView edtPreciob;
    TextView edtResultado;

    String[]titulo=new String[]{
            "Tallarin Saltado",
            "Ceviche Mixto",
            "Pachamanca",
            "Aji de gallina",
            "Cau cau",
            "Arroz Chaufa"
    };
    double[]precios=new double[]{
            20,
            15,
            15,
            19,
            19,
            12
    };
    String[]titulob=new String[]{
            "Inka Cola",
            "Coca Cola",
            "Sprite",
    };
    double[]preciosb=new double[]{
            3.50,
            3.50,
            2.00,
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final GridView lista = (GridView) findViewById( R.id.GridView1 );
        final GridView listab = (GridView) findViewById( R.id.GridView2 );
        edtTitulo=(TextView) findViewById(R.id.edtTitulo);
        edtTitulob=(TextView) findViewById(R.id.edtTituloB);
        edtPrecio=(TextView) findViewById(R.id.edtPrecio);
        edtPreciob=(TextView) findViewById(R.id.edtPrecioB);
        edtResultado=(TextView) findViewById(R.id.edtResultado);
        adapter=new GridViewPlatos(this,titulo,precios);
        lista.setAdapter(adapter);

        adapterb=new GridViewBebidas(this,titulob,preciosb);
        listab.setAdapter(adapterb);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edtTitulo.setText(titulo[position]);
                edtPrecio.setText(String.valueOf(precios[position]));
                Toast.makeText(getApplicationContext(), "costo "+precios[position], Toast.LENGTH_SHORT).show();
            }
        });

        listab.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edtTitulob.setText(titulob[position]);
                edtPreciob.setText(String.valueOf(preciosb[position]));
                Toast.makeText(getApplicationContext(), "costo "+preciosb[position], Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View v) {

    }

    public void onCalcular(View V){
        String num1=edtPrecio.getText().toString();
        String num2=edtPreciob.getText().toString();
        double pp=Double.parseDouble(num1);
        double pb=Double.parseDouble(num2);
        double resultado=0;
        resultado=pp+pb;
        edtResultado.setText("Precio Total: "+resultado);

    }
}
