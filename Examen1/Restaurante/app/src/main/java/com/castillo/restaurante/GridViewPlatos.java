package com.castillo.restaurante;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class GridViewPlatos extends BaseAdapter{
    private Context mContext;
    String[] titulo;
    double[] precio;
    private ArrayList<Integer> listPhoto = new ArrayList<Integer>();

    public GridViewPlatos(Context c,String[] titulo,double[] precio){
        mContext = c;
        this.titulo=titulo;
        this.precio=precio;
        listPhoto.add(R.drawable.tallarin);
        listPhoto.add(R.drawable.ceviche);
        listPhoto.add(R.drawable.pachamanca);
        listPhoto.add(R.drawable.aji);
        listPhoto.add(R.drawable.caucau);
        listPhoto.add(R.drawable.arroz);

    }
    @Override
    public int getCount() {
        return listPhoto.size();
    }

    @Override
    public Object getItem(int position) {
        return listPhoto.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewgroup) {


        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource( listPhoto.get(position) );
        imageView.setScaleType( ImageView.ScaleType.CENTER_CROP );
        imageView.setLayoutParams( new GridView.LayoutParams(100, 120) );
        return imageView;
    }

}
