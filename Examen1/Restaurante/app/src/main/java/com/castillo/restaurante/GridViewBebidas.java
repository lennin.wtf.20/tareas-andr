package com.castillo.restaurante;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class GridViewBebidas extends BaseAdapter{
    private Context mContext;
    String[] titulob;
    double[] preciob;
    private ArrayList<Integer> listPhoto = new ArrayList<Integer>();

    public GridViewBebidas(Context c,String[] titulob,double[] preciob){
        mContext = c;
        this.preciob=preciob;
        this.preciob=preciob;
        //se cargan las miniaturas
        listPhoto.add(R.drawable.coca);
        listPhoto.add(R.drawable.inca);
        listPhoto.add(R.drawable.sprite);

    }
    @Override
    public int getCount() {
        return listPhoto.size();
    }

    @Override
    public Object getItem(int position) {
        return listPhoto.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewgroup) {


        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource( listPhoto.get(position) );
        imageView.setScaleType( ImageView.ScaleType.CENTER_CROP );
        imageView.setLayoutParams( new GridView.LayoutParams(100, 120) );
        return imageView;
    }
}
