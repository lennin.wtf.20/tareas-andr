package com.castillo.calculadora;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextInputEditText edtNum1;
    TextInputEditText edtNum2;
    RadioGroup rbOpcion;
    Button btnCalcular;
    TextView tvResultado;
    TextView tvOperador;
    RadioButton r1;
    RadioButton r2;
    RadioButton r3;
    RadioButton r4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNum1=(TextInputEditText) findViewById(R.id.edtNum1);
        edtNum2=(TextInputEditText)findViewById(R.id.edtNum2);
        rbOpcion=(RadioGroup) findViewById(R.id.rbOpcion);
        btnCalcular=(Button) findViewById(R.id.btnCalcular);
        tvResultado=(TextView) findViewById(R.id.tvResultado);
        tvOperador=(TextView) findViewById(R.id.tvOperador);

        rbOpcion.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                if (checkedId == R.id.rbSum) {
                    tvOperador.setText("+");
                }else if (checkedId == R.id.rbRes) {
                    tvOperador.setText("-");
                }else if (checkedId == R.id.rbProd) {
                    tvOperador.setText("x");
                }else if (checkedId == R.id.rbDiv) {
                    tvOperador.setText("/");
                }
            }

        });

    }

    private boolean validaNumero1()
    {
        String num1 = edtNum1.getText().toString();
        if(num1.isEmpty()){
            edtNum1.setError("Número 1 no puede ser vacío");
            return false;
        } else{
            edtNum1.setError(null);
            return true;
        }
    }

    private boolean validaNumero2()
    {
        String num2 = edtNum2.getText().toString();
        if(num2.isEmpty()){
            edtNum2.setError("Número 2 no puede ser vacío");
            return false;
        } else{
            edtNum2.setError(null);
            return true;
        }
    }
    public void onCalcular(View v) {
        if (!validaNumero1() | !validaNumero2()) {
            return;
        }
        String opcionSeleccionada = String.valueOf(rbOpcion.getCheckedRadioButtonId());
        String num1 = edtNum1.getText().toString();
        String num2 = edtNum2.getText().toString();
        double entero1 = Integer.parseInt(num1);
        double entero2 = Integer.parseInt(num2);
        double res = 0;

        if (tvOperador.getText()=="+") {
            res = entero1 + entero2;
        } else if (tvOperador.getText()=="-") {
            res = entero1 - entero2;
        } else if (tvOperador.getText()=="x") {
            res = entero1 * entero2;
        } else if (tvOperador.getText()=="/") {
            res = entero1 / entero2;
        } else {
            Toast.makeText(this, "Seleccione un operador", Toast.LENGTH_SHORT).show();
        }
        tvResultado.setText(""+res);
    }
}
