package com.example.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    EditText n1 , n2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         n1 = (EditText) findViewById(R.id.edtPnumber);
        n2 = (EditText) findViewById(R.id.edtSnumber);
        Button suma = (Button) findViewById(R.id.btnSuma);
        Button resta = (Button) findViewById(R.id.btnResta);
        Button mult = (Button) findViewById(R.id.btnMultiplica);
        Button div = (Button) findViewById(R.id.btnDivide);

        final TextView  res = (TextView) findViewById(R.id.textResult);
        suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float num1 = new Float(n1.getText().toString());
                float num2 = new Float(n2.getText().toString());
                res.setText("Resultado"+(num1 + num2));
            }

        });
        resta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float num1 = new Float(n1.getText().toString());
                float num2 = new Float(n2.getText().toString());
                res.setText("Resultado"+(num1 - num2));
            }

        });
        mult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float num1 = new Float(n1.getText().toString());
                float num2 = new Float(n2.getText().toString());
                res.setText("Resultado"+(num1 * num2));
            }

        });
        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float num1 = new Float(n1.getText().toString());
                float num2 = new Float(n2.getText().toString());
                res.setText("Resultado"+(num1 / num2));
            }

        });

    }
}
