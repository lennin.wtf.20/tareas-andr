package com.shuan.login.model;

public class Usuario {

        private String nombreUser;
        private  String apellidosUser;
        private  String loginUser;
        private  String claveUser;
        private  String generoUser;
        private  String enteradoUser;
        private  int estadoUser;

        public Usuario(String nombreUser, String apellidosUser, String loginUser, String claveUser, String generoUser, String enteradoUser, int estadoUser) {
                this.nombreUser = nombreUser;
                this.apellidosUser = apellidosUser;
                this.loginUser = loginUser;
                this.claveUser = claveUser;
                this.generoUser = generoUser;
                this.enteradoUser = enteradoUser;
                this.estadoUser = estadoUser;


        }

        public String getNombreUser() {
                return nombreUser;
        }

        public void setNombreUser(String nombreUser) {
                this.nombreUser = nombreUser;
        }

        public String getApellidosUser() {
                return apellidosUser;
        }

        public void setApellidosUser(String apellidosUser) {
                this.apellidosUser = apellidosUser;
        }

        public String getLoginUser() {
                return loginUser;
        }

        public void setLoginUser(String loginUser) {
                this.loginUser = loginUser;
        }

        public String getClaveUser() {
                return claveUser;
        }

        public void setClaveUser(String claveUser) {
                this.claveUser = claveUser;
        }

        public String getGeneroUser() {
                return generoUser;
        }

        public void setGeneroUser(String generoUser) {
                this.generoUser = generoUser;
        }

        public String getEnteradoUser() {
                return enteradoUser;
        }

        public void setEnteradoUser(String enteradoUser) {
                this.enteradoUser = enteradoUser;
        }

        public int getEstadoUser() {
                return estadoUser;
        }

        public void setEstadoUser(int estadoUser) {
                this.estadoUser = estadoUser;
        }
}

//getter and Setter


