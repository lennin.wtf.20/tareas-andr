package com.shuan.login.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.shuan.login.R;

public class LoginActivity extends AppCompatActivity {
    //activando el log
    public static final String TAG ="LoginActivity";
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,  ">>> (DEBUG) Iniciando el metodo onCreate() - PARRA");
        Log.w(TAG,  ">>> (WARNING) Iniciando el metodo onCreate() - PARRA");
        Log.i(TAG,  ">>> (INFO) Iniciando el metodo onCreate() - PARRA");
        Log.e(TAG,  ">>> (ERROR) Iniciando el metodo onCreate() - PARRA");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }
}
